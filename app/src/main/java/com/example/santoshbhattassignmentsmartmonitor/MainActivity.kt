package com.example.santoshbhattassignmentsmartmonitor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TableLayout
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.iterator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.santoshbhattassignmentsmartmonitor.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    private var tableLayout:TableLayout? = null
    private var recyclerView:RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val imagesList = ArrayList<Int>()

        //initializing views
        tableLayout = binding.tlTableView
        recyclerView = binding.rvImages

        //changing color of the table rows
        var counter = 0
        tableLayout?.iterator()?.forEach { tableRow->
            if (counter != 0 && counter %2 == 0){
                tableRow.background = AppCompatResources.getDrawable(this,R.drawable.table_row_background1)
            }else if(counter %2 != 0){
                tableRow.background = AppCompatResources.getDrawable(this,R.drawable.table_row_background2)
            }
            counter++
        }

        //adding images to the arraylist
        imagesList.add(R.drawable.cr7_wallpaper_for_desktop_1024x640)
        imagesList.add(R.drawable.ronaldinho_wallpaper)
        imagesList.add(R.drawable.cristiano_ronaldo_wallpaper_6_576x1024)
        imagesList.add(R.drawable.photo_messi)
        imagesList.add(R.drawable.wallpaper_ronaldinho)
        imagesList.add(R.drawable.cristiano_ronaldo_pictures_2020_473x1024)
        imagesList.add(R.drawable.messi_wallpaper_free_download)
        imagesList.add(R.drawable.wallpaper_messi)
        imagesList.add(R.drawable.wallpaper_of_messi)
        imagesList.add(R.drawable.ronaldinho)


        recyclerView?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity,LinearLayoutManager.HORIZONTAL,false)
            adapter = AdapterRecycler(this@MainActivity,imagesList)
        }

    }
}