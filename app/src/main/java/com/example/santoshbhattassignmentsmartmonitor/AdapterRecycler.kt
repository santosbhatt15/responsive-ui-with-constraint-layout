package com.example.santoshbhattassignmentsmartmonitor

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.santoshbhattassignmentsmartmonitor.databinding.ImagesItemViewBinding
import com.google.android.material.imageview.ShapeableImageView

class AdapterRecycler(private val context: Context, private val imagesList: ArrayList<Int>)
    : RecyclerView.Adapter<AdapterRecycler.ViewHolder>() {

    inner class ViewHolder(binding: ImagesItemViewBinding):RecyclerView.ViewHolder(binding.root){
        val imageView:ShapeableImageView = binding.imageView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ImagesItemViewBinding.inflate(LayoutInflater.from(parent.context) ,parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(imagesList[position]).into(holder.imageView)
    }

    override fun getItemCount(): Int = imagesList.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

}
